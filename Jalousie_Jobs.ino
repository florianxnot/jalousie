
#include <EEPROM.h>
#include "Wire.h"
#define DS3231_ADDRESSE 0x68

#define EEPROM_WORK_LEVEL_ADDRESS 100

#define JOB_DOWN 0
#define JOB_UP 1
#define JOB_WORK 2

//#define TASK_TIMER 't'-'0'

#define NUMB_OF_JOBS 16


#define REL_1 8
#define REL_2 9


char data = 0;            //Variable for storing received data

byte work_level = -1;
byte is_on = 1;

struct ENTITY {
  bool alive;
  bool active;
  bool self_destroying;
  byte number;
  byte task;
  byte day;
  byte hour;
  byte minute;
  
};


ENTITY job[NUMB_OF_JOBS];


byte sekunde, minute, stunde, wochentag, tag, monat, jahr;

void setup()
{
    Wire.begin();
    Serial.begin(9600);   //Sets the baud for serial data transmission                               
    pinMode(REL_1, OUTPUT);  //Sets digital pin REL_1 as output pin
    pinMode(REL_2, OUTPUT);  //Sets digital pin REL_2 as output pin
    pinMode(13, OUTPUT);  //Sets digital pin 13 as output pin
    digitalWrite(REL_1, LOW);   //If value is 1 then RELAY1 turns ON
    digitalWrite(REL_2, LOW);   //If value is 1 then RELAY2 turns ON
    work_level = EEPROM.read(EEPROM_WORK_LEVEL_ADDRESS);
    read_all_EEPROM();
    Serial.println(job[0].alive);
    Serial.println(job[0].active);
    Serial.println(job[0].number);
    Serial.println(job[0].task);
    Serial.println(job[0].day);
    Serial.println(job[0].hour);
    Serial.println(job[0].minute);
}
void loop()
{  
  if(is_on){//ON
   leseDS3231zeit(&sekunde, &minute, &stunde, &wochentag, &tag, &monat, &jahr); 
   for(int i = 0; i< NUMB_OF_JOBS; i++){
     if(job[i].alive && job[i].active){        // job lebt und ist active
         if( (job[i].day == wochentag || job[i].day == 't'-'0' || job[i].day == 9 || (job[i].day == 7 &&wochentag <=4)|| (job[i].day == 8 &&wochentag >=5))&& job[i].hour == stunde && job[i].minute == minute){
            switch(job[i].task){  
              case JOB_DOWN:{
                 digitalWrite(REL_1, HIGH);   //If v alue is 1 then LED turns ON
                 digitalWrite(REL_2, HIGH);  //If value is 1 then LED turns ON
                 for(int i = 0; i< 65; i++){
                   digitalWrite(13,HIGH);
                   delay(500);
                   digitalWrite(13,LOW);
                   delay(500);
                 }
                 digitalWrite(REL_1, LOW);   //If value is 1 then LED turns ON
                 digitalWrite(REL_2, LOW);  //If value is 1 then LED turns ON
                 break;
               }
               case JOB_UP:{
                 digitalWrite(REL_1, HIGH);   //If value is 1 then LED turns ON
                 digitalWrite(REL_2, LOW);   //If value is 1 then LED turns ON
                 for(int i = 0; i< 65; i++){
                   digitalWrite(13,HIGH);
                   delay(500);
                   digitalWrite(13,LOW);
                   delay(500);
                 }
                 digitalWrite(REL_1, LOW);   //If value is 1 then LED turns ON
                 digitalWrite(REL_2, LOW);   //If value is 1 then LED turns ON
                 break;
               }
               case JOB_WORK:{
                 long level_int_temp = 0; 
                 level_int_temp = (long)work_level; 
                 int down_mil_sec_temp = 20000 - ((level_int_temp*20000)/63); 
                 down();
                 Serial.print("down for ");
                 Serial.print(down_mil_sec_temp/1000);
                 Serial.print(" seconds\n"); 
                 delay(down_mil_sec_temp);
                 nothing();
                 Serial.print("nothing for ");
                 Serial.print((61000 - down_mil_sec_temp)/1000);
                 Serial.print(" seconds\n");
                 delay(61000-down_mil_sec_temp);
                 break;}
               default:{
                 Serial.print("Error Loop\n");
                 //error 404 job not found 
                 break;
               }
             }
             if(job[i].self_destroying){
               job[i].alive = 0;
               byte two =  EEPROM.read(i*4+1); // XXXX XXXX
               EEPROM.write(i*4+1,two&0x7F); //0XXX XXXX
             }
         }
     }
   }
  }
  else{//OFF
     
  }
  delay(500);
}
void serialEvent(){
  
   if(Serial.available() > 0)      // Send data only when you receive data:
   {
      data = Serial.read();        //Read the incoming data & store into data
      //Serial.print(data);          //Print Value inside data in Serial monitor
      //Serial.print("\n");

     switch(data){
       case 'f':{      //flip turn on/off
        delay(20);
        if(Serial.available() > 0)      
        {
          data = Serial.read();
          if(data == '0'){
            flip(0);
          }else if(data == '1'){
            flip(1);
          }else if(data == '2'){
            flip(2);
          }else if(data == '3'){
            flip(3);  
          }else if(data == '4'){
            flip(4);    
          }else if(data == '5'){
            flip(5);      
          }else if(data == '6'){
            flip(6);        
          }else if(data == '7'){
            flip(7);          
          }else if(data == '8'){
            flip(8);            
          }else if(data == '9'){
            flip(9);              
          }else if(data == 'A'){
            flip(10);                
          }else if(data == 'B'){
            flip(11);                 
          }else if(data == 'C'){
            flip(12);                    
          }else if(data == 'D'){
            flip(13);                      
          }else if(data == 'E'){
            flip(14);                        
          }else if(data == 'F'){
            flip(15);                          
          }else{
            //error
          }
        }
        break;
      }  
      case 'n':{                           //noting
         nothing();
         Serial.print("nothing");
         Serial.print("\n"); 
         break;
      }   
      case 'u':{                            //  up
         up();
         Serial.print("up");
         Serial.print("\n"); 
         break;
      }
      case 'd':{                             //down
         down();
         Serial.print("down");
         Serial.print("\n"); 
         break;
      }
      case 'b':{                             //bottom
         down();
         Serial.print("down to the bottom");
         Serial.print("\n"); 
         delay(25000);
         nothing();
         Serial.print("nothing");
         Serial.print("\n");
         break;
       }
       case 't':{                            //top
         up();
         Serial.print("up to top");
         Serial.print("\n"); 
         delay(27000);
         nothing();
         Serial.print("nothing");
         Serial.print("\n");
         break;
       }
       case 'w':{                            //work
         long level_int_temp = 0; 
         level_int_temp = (long)work_level; 
         int down_mil_sec_temp = 20000 - ((level_int_temp*20000)/63); 
         down();
         Serial.print("down for ");
         Serial.print(down_mil_sec_temp/1000);
         Serial.print(" seconds\n"); 
         delay(down_mil_sec_temp);
         nothing();
         Serial.print("nothing");
         Serial.print("\n");
         break;
        }
       case 'g':{                          //GET
        delay(20);
           if(Serial.available() > 0)      
           {
             data = Serial.read();
             if(data == 'w'){
               show_work_level();
             }else if(data == 't'){
              zeigeZeit();
             }else if(data == 'a'){
               get_numb_active_jobs();
             }else if(data == 'n'){
               get_numb_jobs();
             }else if(data == 'j'){
               get_job();
             }else{
              Serial.println("possible input for g is: gt for getting clock time, gu for getting number of jobs, ga for getting number of active jobs, gw for getting work level");  
             }
           }else{
            Serial.println("possible input for g is: gt for getting clock time, gu for getting number of jobs, ga for getting number of active jobs, gw for getting work level"); 
           } 
           break;
      }
       case 's':{                //SET
        delay(20);
           if(Serial.available() > 0)      
           {
             data = Serial.read();
            /* if(data == 'u'){
               set_up_time();
             }else if(data == 'd'){
               set_down_time();
             }else */if(data == 'w'){
              set_work_level();
             }else if(data == 't'){
              set_time();
             }else{
              Serial.println("possible input for s is: st for setting time, su for setting time up, sd for setting time down, sw for setting work level");  
             }
           }else{
            Serial.println("possible input for s is: st for setting time, su for setting time up, sd for setting time down, sw for setting work level");  
           }
         break;
      }
      case 'a':{                //add new job
        add_job();
        break;
      }
      case 'c':{                //cancel job
        delete_job();
        break;
      }
      case 'r':{                //remove job
        read_function();
        break;
      }
      case 'l':{              // löschen delete all
        delete_all();
      }
      /*case 'h':{
        if(65 <= data && data <=122){
         data = -1;
         Serial.write("Help-Function\n");
         delay(100);
         Serial.write("Possible Commands:\n");
         delay(100);
         Serial.write("f   - flip ON/OFF switch for job number X\n");
         delay(100);
         Serial.write("st  - set time of clock\n");
         delay(100);
         Serial.write("sw  - set Work-level\n");
         delay(100);
         Serial.write("gt  - get time of clock\n");
         delay(100);
         Serial.write("gw  - get Work-level\n");
         delay(100);
         Serial.write("ga  - get number of active Jobs\n");
         delay(100);
         Serial.write("gn  - get number of jobs\n");
         delay(100);
         Serial.write("l   - delete all EEPROM\n");
         delay(100);
         Serial.write("r   - read jobs\n");
         delay(100);
         Serial.write("a   - add job\n");
         delay(100);
         Serial.write("c   - cancel job\n");
         delay(100);
         Serial.write("u   - drive motor up\n");
         delay(100);
         Serial.write("d   - drive motor down\n");
         delay(100);
         Serial.write("n   - stop motor, do nothing\n");
         delay(100);
         Serial.write("t   - go to top\n");
         delay(100);
         Serial.write("b   - go to bottom\n");
         delay(100);
         Serial.write("h   - open help function\n");
         delay(100);
       }
      }*/
      default:{
        break;
      }  
      }
    }
  
}
