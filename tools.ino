void get_job(){
  delay(20);
  int i = 0;
  byte digit[2]; 
  while(Serial.available() > 0)      // Send data only when you receive data:
  {        
       data = Serial.read();
       digit[i] = data - '0';
       Serial.print(digit[i]);
       delay(20);
       i++; 
  }
  if(i == 2){
    byte numb_temp = digit[0] * 10 + digit[1]; 
    read_Job_EEPROM((int)numb_temp);
    
  }else{
   Serial.print("Wrong Format, type: gjXX");
    
  }
}
void read_function(){
  delay(20);
  int i = 0;
  byte digit[2]; 
  while(Serial.available() > 0)      // Send data only when you receive data:
  {        
       data = Serial.read();
       if(data == 'a'){
         read_job_all();
         return;
       }else{
         digit[i] = data - '0';
       }
       delay(20);
       i++; 
  }
  if(i == 2){
    byte numb_temp = digit[0] * 10 + digit[1]; 
    read_job(numb_temp);
    
  }else{
   Serial.print("Wrong Format, type: ra or rXX");
  }
}
void read_job_all(){
  for(int i = 0; i<NUMB_OF_JOBS;i++){
    read_job(i);
  }
}
void read_job(int numb){
  Serial.print("job number: ");
  Serial.println(numb);  
  Serial.print("alive: ");
  Serial.println(job[numb].alive);
  Serial.print("active: ");
  Serial.println(job[numb].active);
  Serial.print("task: ");
  if(job[numb].task == JOB_DOWN){
    Serial.println("d");
  }else if(job[numb].task == JOB_UP){
    Serial.println("u");
  }else if(job[numb].task == JOB_WORK){
    Serial.println("w");
  }
  Serial.print("day: ");
  print_day(job[numb].day); 
  Serial.print("\n");
  Serial.print("time: ");
  Serial.print(job[numb].hour);
  Serial.print(":");
  Serial.println(job[numb].minute);
}
void delete_all(){
  Serial.println("delete all");
  for(int i = 0; i<4*NUMB_OF_JOBS;i++)
  {
    EEPROM.write(i,0);
    
  }
  read_all_EEPROM();
}
void delete_job(){
  delay(20);
  int i = 0;
  byte digit[2];
  while(Serial.available() > 0)      // Send data only when you receive data:
  {        
       data = Serial.read();
       digit[i] = data - '0';
       Serial.print(digit[i]);
       delay(20);
       i++; 
  }
  if(i == 2){
       
    byte numb = digit[0]*10 + digit[1];
    job[numb].alive = 0;
    byte two =  EEPROM.read(numb*4+1); // XXXX XXXX
    EEPROM.write(numb*4+1,two&0x7F); //0XXX XXXX
    
  }else{
   Serial.println("wrong format: you should enter cXX and XX is number between 00 and 16"); 
    
  }
   
}
void add_job(){
  delay(20);
  int i = 0;
  int j = 0;
  byte digit[6]; //TDHHMM
  while(Serial.available() > 0)      // Send data only when you receive data:
  {        
       data = Serial.read();
       digit[i] = data - '0';
       Serial.print(digit[i]);
       delay(20);
       i++; 
  }
  if(i == 6){
    
    byte task_temp = digit[0];    
    byte day_temp = digit[1]; //Monday = 0, Sunday = 6, Week = w, weekend = e, everyday = a, timer = t
    if(day_temp == 'w'-'0'){
      day_temp = 7;
    }else if(day_temp == 'e'-'0'){
      day_temp = 8;
    }else if(day_temp == 'a'-'0'){
      day_temp = 9;
    }
    
    byte hour_temp = digit[2] * 10 + digit[3]; 
    //Serial.print("hour_temp: ");
    //Serial.println(hour_temp);
    byte minute_temp = digit[4] * 10 + digit[5];
    //Serial.print("minute_temp: ");
    //Serial.println(minute_temp);
    if(task_temp == 'b'-'0' || task_temp == 'd'-'0' ){
     task_temp = JOB_DOWN;
    } else if(task_temp == 't'-'0' || task_temp == 'u'-'0' ){
     task_temp = JOB_UP;
    } else if(task_temp == 'w'-'0'){
     task_temp = JOB_WORK;
    } 
    if(0<= task_temp && task_temp <=2 && hour_temp <= 23 && 0 <= hour_temp && 0<= minute_temp && minute_temp <= 59){
      
      for(j = 0; j<= NUMB_OF_JOBS;j++){
        if(job[j].alive == 0){
         break; 
        }
      }
      if (j != NUMB_OF_JOBS){
        
        if(day_temp != 't'- '0'){ // normaler modus, kein timing
          job[j].alive = 1;
          activate_job(j);
          job[j].task = task_temp;
          job[j].day = day_temp;
          job[j].hour = hour_temp;
          job[j].minute = minute_temp;
          job[j].self_destroying = 0;
          job[j].number = j;
        
        
          EEPROM.write(j*4,(j<<5) + task_temp); //NNNN 00JJ 
          EEPROM.write(j*4+1,1*0x80 + 1*0x40 +day_temp); //1100 0DDD 
          Serial.print(j*4+1,1*0x80 + 1*0x40 +day_temp);
          EEPROM.write(j*4+2,hour_temp); //000H HHHH
          EEPROM.write(j*4+3,minute_temp); //00MM MMMM 
          
          Serial.write("\n Job added - numb: "); 
          Serial.print(j);
          Serial.write("\n");
          
        }else{ //timing
          leseDS3231zeit(&sekunde, &minute, &stunde, &wochentag, &tag, &monat, &jahr);   // Daten vom DS3231 holen
          delay(20);
          job[j].alive = 1;
          activate_job(j);
          job[j].task = task_temp;
          job[j].day = day_temp;
          job[j].hour = stunde + hour_temp;
          job[j].minute = minute + minute_temp;
          if(job[j].minute >= 60 ){
           job[j].hour++;
           job[j].minute -= 60;
           if(job[j].hour >=24){
             job[j].hour-=24;
           }
          }
          job[j].self_destroying = 1;
          Serial.println("hour - minute");
          Serial.print(job[j].hour);
          Serial.print(" - ");
          Serial.println(job[j].minute);
        }
        
      }else{
        Serial.write("No Job available\n"); 
      }
         
    }else{
        Serial.println(" No legal input. "); 
        Serial.println("NO job added!");
    }
           

  }
  else{
       Serial.println(" Wrong Format!  You should enter aTDHHMM, example:");
       for(int j = 0;j<i;j++){
         Serial.print(digit[j]);
       }
  }
  
}
void read_all_EEPROM(){
  int i;
  byte one,two,three,foor;
  for(i = 0; i< NUMB_OF_JOBS; i++){
    one =  EEPROM.read(i*4);
    two =  EEPROM.read(i*4+1);
    three =  EEPROM.read(i*4+2);
    foor =  EEPROM.read(i*4+3);
    job[i].alive = (two & 0x80); //nicht sicher von der logik
    job[i].active = (two & 0x40);//nicht sicher von der logik
    job[i].number = (one & 0xF0)>>4;
    job[i].task = one & 0x0F;
    job[i].day = two & 0x0F;
    job[i].hour = three;
    job[i].minute = foor;
    
  }
  
}
void delete_Job(int numb){
  
  job[numb].alive = 0;
  
  EEPROM.write(numb*4+1,(job[numb].active)&0x40 + (job[numb].day)&0x0F); //0A00 DDDD
  
  
}
void read_Job_EEPROM(int numb){
  Serial.println("read_job_eeprom");
  Serial.println(EEPROM.read(numb*4));
  Serial.println(EEPROM.read(numb*4+1));
  Serial.println(EEPROM.read(numb*4+2));
  Serial.println(EEPROM.read(numb*4+3));
  
  
}
void get_numb_jobs(){
 int count = 0;
  
 for(int i = 0; i< NUMB_OF_JOBS; i++){
   
   if (job[i].alive){ 
     count++; 
   }
   
 }
    
 Serial.print(count);
 Serial.println(" jobs alive");
  
}
void get_numb_active_jobs(){
 int count = 0;
  
 for(int i = 0; i< NUMB_OF_JOBS; i++){
   
   if (job[i].alive){ 
     if(job[i].active){
       count++;
     } 
   }
 }  
 Serial.print(count);
 Serial.println(" jobs active");
 
}
void activate_job(int numb){
  
  job[numb].active = true;
  
  EEPROM.write(numb*4+1,(job[numb].alive)*0x80 + 1*0x40 +(job[numb].day)&0x0F); //A100 DDDD  
  
}
void deactivate_job(int numb){
  job[numb].active = false;
  
  EEPROM.write(numb*4+1,(job[numb].alive)*0x80 + 0*0x40 +(job[numb].day)&0x0F); //A100 DDDD  
  
}

void show_work_level(){
  
  Serial.write("Worklevel : ");
  Serial.println(work_level);
  
}
void set_work_level(){
  delay(20);
  int i = 0;
  byte digit[2];
  while(Serial.available() > 0)      // Send data only when you receive data:
  {        
       data = Serial.read();
       digit[i] = data - '0';
       Serial.print(digit[i]);
       delay(20);
       i++; 
  }
  if(i == 2){
       
    byte level_temp = digit[0]*10 + digit[1];
   
       
    if(level_temp <=63 && 0 <=level_temp){
         work_level = level_temp;
         byte eepromdown_level = EEPROM.read(EEPROM_WORK_LEVEL_ADDRESS);
         if(eepromdown_level != work_level){
           EEPROM.write(EEPROM_WORK_LEVEL_ADDRESS,work_level);  
         }
         Serial.write("\n Work Level is set: "); 
         Serial.print(work_level);
         Serial.write("\n");
         
    }else{
        Serial.println(" No legal level. level has to be between 0 and 99 "); 
        Serial.println("Work-lev was NOT set!");
    }
           

  }
  else{
       Serial.println(" Wrong Format! You should enter two digits. Anumber between 0 and 63 ");
       for(int j = 0;j<i;j++){
         Serial.print(digit[j]);
       }
  }
}

void set_time(){
  delay(20);
  int i = 0;
  byte date[14];
  while(Serial.available() > 0)      // Send data only when you receive data:
  {
             
       data = Serial.read();
       date[i] = data - '0';
       Serial.print(date[i]);
       delay(20);
       i++; 
  }
  if(i == 14){
       byte sec = date[0] * 10 + date[1];
       byte min = date[2] * 10 + date[3];
       byte hour = date[4] * 10 + date[5];
       byte wd = date[6] * 10 + date[7];
       byte day = date[8] * 10 + date[9];
       byte mon = date[10] * 10 + date[11];
       byte year = date[12] * 10 + date[13];
       einstellenDS3231zeit(sec, min, hour, wd, day, mon, year);
       Serial.print("Please wait\n");
       Serial.print("Countdown start:\n");
       Serial.print("4\n");
       delay(1000);
       Serial.print("3\n");
       delay(1000);
       Serial.print("2\n");
       delay(1000);
       Serial.print("1\n");
       delay(1000);
           
       Serial.print("\n");
  }
  else{
       Serial.println(" Wrong Format! You should enter: SSMMHHWWDDMMYY\n You wrote: ");
  }
  for(int j = 0;j<i;j++){
       Serial.print(date[j]);
  }
  Serial.write("\n Time is set\n");  
  
}
void einstellenDS3231zeit(byte sekunde, byte minute, byte stunde, byte wochentag, byte tag, byte monat, byte jahr) {
      // Datum und Uhrzeit einstellen
      Wire.beginTransmission(DS3231_ADDRESSE);
      Wire.write(0);
      Wire.write(decToBcd(sekunde)); // Sekunden einstellen
      Wire.write(decToBcd(minute)); // Minuten einstellen
      Wire.write(decToBcd(stunde));
      Wire.write(decToBcd(wochentag)); // 1=Sonntag ... 7=Samstag
      Wire.write(decToBcd(tag));
      Wire.write(decToBcd(monat));
      Wire.write(decToBcd(jahr)); // 0...99
      Wire.endTransmission();
}
void leseDS3231zeit(byte *sekunde, byte *minute,byte *stunde, byte *wochentag, byte *tag, byte *monat, byte *jahr) {
      Wire.beginTransmission(DS3231_ADDRESSE);
      Wire.write(0); // DS3231 Register zu 00h
      Wire.endTransmission();
      Wire.requestFrom(DS3231_ADDRESSE, 7); // 7 Byte Daten vom DS3231 holen
      *sekunde = bcdToDec(Wire.read() & 0x7f);
      *minute = bcdToDec(Wire.read());
      *stunde = bcdToDec(Wire.read() & 0x3f);
      *wochentag = bcdToDec(Wire.read());
      *tag = bcdToDec(Wire.read());
      *monat = bcdToDec(Wire.read());
      *jahr = bcdToDec(Wire.read());
    }
void zeigeZeit(){
      leseDS3231zeit(&sekunde, &minute, &stunde, &wochentag, &tag, &monat, &jahr);   // Daten vom DS3231 holen
      if (tag < 10) { Serial.print("0");} 
      Serial.print(tag); // ausgeben T.M.J H:M:S
      Serial.print(".");
      if (monat < 10) { Serial.print("0");}
      Serial.print(monat);
      Serial.print(".20");
      Serial.print(jahr);
      Serial.print(" ");
      if (stunde < 10) { Serial.print("0");}
      Serial.print(stunde, DEC); // byte in Dezimal zur Ausgabe
      Serial.print(":");
      if (minute < 10) { Serial.print("0");}
      Serial.print(minute, DEC);
      Serial.print(":");
      if (sekunde < 10) { Serial.print("0"); }
      Serial.print(sekunde, DEC);
      Serial.print("\n");
      
      Serial.print("Weekday: (");
      Serial.print(wochentag, DEC);
      Serial.print(") ");
      print_day(wochentag);
    
}
byte decToBcd(byte val) {
    // Dezimal Zahl zu binary coded decimal (BCD) umwandeln
      return((val/10*16) + (val%10));
}
byte bcdToDec(byte val) {
    // BCD (binary coded decimal) in Dezimal Zahl umwandeln
      return((val/16*10) + (val%16));
}
void down(){
  digitalWrite(13, HIGH);    //If value is 0 then LED turns OFF
  digitalWrite(REL_1, HIGH);   //If value is 1 then LED turns ON
  digitalWrite(REL_2, HIGH);  //If value is 1 then LED turns ON
}
void up(){
  digitalWrite(13, HIGH);    //If value is 0 then LED turns OFF
  digitalWrite(REL_1, HIGH);   //If value is 1 then LED turns ON
  digitalWrite(REL_2, LOW);   //If value is 1 then LED turns ON
}
void nothing(){
  digitalWrite(13, LOW);   //If value is 1 then LED turns ON
  digitalWrite(REL_1, LOW);   //If value is 1 then LED turns ON
  digitalWrite(REL_2, LOW);   //If value is 1 then LED turns ON
}
void flip(int number){
  if(job[number].active == 1){
   job[number].active = 0;
   Serial.print(number);
   Serial.print(" Automatic Jalousie OFF");
   Serial.print("\n");  
  }
  else if(job[number].active == 0){
   job[number].active = 1; 
   Serial.print(number);
   Serial.print(" Automatic Jalousie ON");
   Serial.print("\n"); 
  }
  
}
void print_day(byte day){
  switch (day) {
  case 0:
    Serial.print("Mon");
    break;
  case 1:
    Serial.print("Tue");
    break;
  case 2:
    Serial.print("Wed");
    break;
  case 3:
    Serial.print("Thu");
    break;
  case 4:
    Serial.print("Fri");
    break;
  case 5:
    Serial.print("Sat");
    break;
  case 6:
    Serial.print("Son");
    break;
  case 7:
    Serial.print("Week");
    break;
  case 8:
    Serial.print("Weekend");
    break;  
  case 9:
    Serial.print("Allday");
    break; 
  case 't'-'0':
    Serial.print("timer");
    break;  
  default:
    Serial.print("Error no legal input: ");
    Serial.println(day);
    break;
  }
  
}
